import firebase from 'firebase'

const firebaseConfig = {
	apiKey: "AIzaSyAqtGnjAudvUuQJ8d8f3B5yt0BSZBGVhoU",
	authDomain: "time2plan-0001.firebaseapp.com",
	databaseURL: "https://time2plan-0001.firebaseio.com",
	projectId: "time2plan-0001",
	storageBucket: "time2plan-0001.appspot.com",
	messagingSenderId: "1098827443",
	appId: "1:1098827443:web:291ea5d9de226a87f512c6",
	measurementId: "G-LRV22VEZ3L"
};

firebase.initializeApp(firebaseConfig);

export default firebase;