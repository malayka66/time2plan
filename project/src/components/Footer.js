import React from 'react';
import {
	Divider, Icon
} from 'semantic-ui-react';

import { pullRight, h1 } from './Footer.scss';
const Footer = () => {
	return (
		<div>
			<Divider />
			<p className={pullRight}>
				Made with <Icon name="heart" color="red" /> by Esau Silva
      		</p>
		</div>
	);
};


export default Footer;