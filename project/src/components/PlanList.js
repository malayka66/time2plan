import React, { useState, useEffect } from "react"
import CardDeck from "react-bootstrap/CardDeck"
import Card from "react-bootstrap/Card"
import Plan from "./Plan"

const PlanList = () => {
	const [plans, setPlans] = useState([
		{
			id: 1,
			planTitle: "Paris",
			archived: false
		},
		{
			id: 2,
			planTitle: "Londres",
			archived: false
		}, {
			id: 3,
			planTitle: "Paris",
			archived: false
		},
		{
			id: 4,
			planTitle: "Londres",
			archived: false
		},
		{
			id: 5,
			planTitle: "Paris",
			archived: false
		},
		{
			id: 6,
			planTitle: "Londres",
			archived: false
		},
		{
			id: 7,
			planTitle: "Paris",
			archived: false
		},
		{
			id: 8,
			planTitle: "Londres",
			archived: false
		}

	])

	// useEffect(() => {
	// 	setPlans([
	// 		{
	// 			planTitle: "Paris",
	// 			archived: false
	// 		},
	// 		{
	// 			planTitle: "Londres",
	// 			archived: false
	// 		}
	// 	])
	// }, [])

	return (
		<CardDeck>
			<Card className="plus" key="addPlan" onClick={() => { }} border="light">
				<Card.Body>
					<span>+</span>
				</Card.Body>
			</Card>
			{plans.map((plan) =>
				<Plan key={plan.id} data={plan} />
			)}
		</CardDeck>
	);
};

export default PlanList;