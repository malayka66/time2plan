import React, { useState, useEffect } from "react"
import "./Day.scss"

const Day = ({ data }) => {

	return (
		<div className="day">
			<div className="section topSection"></div>
			<div className={(data.dividers.top ? "divider" : "divider hidden")}></div>
			<div className="section midSection"></div>
			<div className={(data.dividers.mid ? "divider" : "divider hidden")}></div>
			<div className="section midSection"></div>
			<div className={(data.dividers.bot ? "divider" : "divider hidden")}></div>
			<div className="section bottomSection"></div>
		</div>
	);
};

export default Day;