import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import { Nav, Navbar, NavDropdown } from 'react-bootstrap';

const MenuApp = ({ }) => {
	const [userName, setUser] = useState("panda")
	return (
		<Navbar sticky="top" fixed="top">
			<Nav defaultActiveKey="/">
				<Nav.Item eventKey="/">
					<Link className="nav-link" to="/"> Home</Link>
				</Nav.Item>
				<Nav.Item eventKey="/createPlan">
					<Link className="nav-link" to="/createPlan"> CreatePlan</Link>
				</Nav.Item>

				<Nav.Item eventKey="/profile">
					<Link className="nav-link" to="/profile"> Profile</Link>
				</Nav.Item>

				<NavDropdown title={userName} id="nav-dropdown">
					<NavDropdown.Item eventKey="username.settings">Settings</NavDropdown.Item>
					<NavDropdown.Divider />
					<NavDropdown.Item eventKey="username.logout">Logout</NavDropdown.Item>
				</NavDropdown>
			</Nav>
		</Navbar>
	);
};


export default MenuApp;