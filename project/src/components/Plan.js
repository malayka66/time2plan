import React, { useState, useEffect } from "react"
import Card from "react-bootstrap/Card"
import "./Plan.scss"

const Plan = ({ data }) => {

	return (
		<Card border="light">
			<Card.Body>
				{data.planTitle}
			</Card.Body>
		</Card>
	);
};

export default Plan;