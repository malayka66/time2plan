import React, { useState } from 'react';

import Layout from './Layout';
import PlanList from '../components/PlanList';
import Day from '../components/Day';

const Home = () => {

	const [data, setData] = useState({
		dividers: {
			top: false,
			mid: true,
			bot: false
		}
	})

	return (
		<Layout title="Home">
			<PlanList />
			{/* 
			<Day data={data} /> */}
		</Layout>
	);
};

export default Home;