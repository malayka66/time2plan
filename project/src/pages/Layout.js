import React from 'react';
import MenuApp from '../components/MenuApp'
import Footer from '../components/Footer'

import { Container, Row, Col } from 'react-bootstrap';

import './Layout.scss';

const Layout = ({ children, title }) => {

	return (
		<Container fluid>
			<Row>
				<Col id="menu">
					<MenuApp />
				</Col>
			</Row>
			<Row className="v100">
				<Col>
					<div className="title">
						<h1>{title}</h1>
					</div>
					<div className="main-container">
						{children}
					</div>
				</Col>
			</Row>
		</Container>
	);
};

export default Layout;