import React from 'react';
import { Switch, BrowserRouter as Router, Route } from 'react-router-dom';
import importedComponent from 'react-imported-component';
import './_app.scss';
import Home from './Home';
import CreatePlan from './CreatePlan';
import Loading from '../components/Loading';

const AsyncDynamicPage = importedComponent(
	() => import('./DynamicPage'),
	{
		LoadingComponent: Loading
	}
);
const AsyncNoMatch = importedComponent(
	() => import('./NoMatch'),
	{
		LoadingComponent: Loading
	}
);

const AsyncCreatePlan = importedComponent(
	() => import('./CreatePlan'),
	{
		LoadingComponent: CreatePlan
	}
);

const App = () => {
	return (
		<Router>
			<Switch>
				<Route exact path="/" component={Home} />
				<Route exact path="/createPlan" component={AsyncCreatePlan} />
				<Route component={AsyncNoMatch} />
			</Switch>
		</Router>
	);
};

export default App;