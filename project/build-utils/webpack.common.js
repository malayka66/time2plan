const commonPaths = require('./common-paths');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const config = {
	entry: 'index.js',
	output: {
		path: commonPaths.outputPath,
		publicPath: '/'
	},
	module: {
		rules: [
			{
				test: /\.(js)$/,
				exclude: /node_modules/,
				use: ['babel-loader']
			},
			{
				test: /\.(scss)$/,
				use: [{
					loader: 'style-loader', // inject CSS to page
				},
				{
					loader: 'css-loader', // translates CSS into CommonJS modules
				},
				{
					loader: 'sass-loader' // compiles Sass to CSS
				}]
			}
		]
	},
	optimization: {
		splitChunks: {
			cacheGroups: {
				styles: {
					name: 'styles',
					test: /\.css$/,
					chunks: 'all',
					enforce: true
				}
				// vendor: {
				// 	chunks: 'initial',
				// 	test: 'vendor',
				// 	name: 'vendor',
				// 	enforce: true
				// }
			}
		}
	},
	stats: {
		children: false,
	},
	plugins: [
		new HtmlWebpackPlugin({
			template: 'public/index.html'
		})
	]
};
module.exports = config;